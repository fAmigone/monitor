<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monchartotal".
 *
 * @property integer $idpregunta
 * @property string $pregunta
 * @property string $count(idrespuesta)
 * @property string $respuesta
 * @property integer $idmateria
 * @property string $materia
 * @property integer $idcarrera
 * @property string $carrera
 * @property string $fecha
 */
class Monchartotal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monchartotal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idpregunta', 'cantidad', 'idmateria', 'idcarrera'], 'integer'],
            [['pregunta', 'respuesta', 'idmateria', 'materia', 'idcarrera', 'carrera', 'fecha'], 'required'],
            [['pregunta'], 'string', 'max' => 500],
            [['respuesta'], 'string', 'max' => 2000],
            [['materia', 'carrera'], 'string', 'max' => 300],
            [['fecha'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idpregunta' => 'Idpregunta',
            'pregunta' => 'Pregunta',
            'count(idrespuesta)' => 'Cantidad',
            'respuesta' => 'Respuesta',
            'idmateria' => 'Idmateria',
            'materia' => 'Materia',
            'idcarrera' => 'Idcarrera',
            'carrera' => 'Carrera',
            'fecha' => 'Fecha',
        ];
    }
     
}
