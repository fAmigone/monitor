<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monresultado;

/**
 * MonresultadoSearch represents the model behind the search form about `app\models\Monresultado`.
 */
class MonresultadoSearch extends Monresultado
{
    public function rules()
    {
        return [
            [['id', 'idmonresultadocab', 'idpregunta', 'idrespuesta'], 'integer'],
            [['libre'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Monresultado::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idmonresultadocab' => $this->idmonresultadocab,
            'idpregunta' => $this->idpregunta,
            'idrespuesta' => $this->idrespuesta,
        ]);

        $query->andFilterWhere(['like', 'libre', $this->libre]);

        return $dataProvider;
    }
}
