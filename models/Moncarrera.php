<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "moncarrera".
 *
 * @property integer $id
 * @property string $nombre
 */
class Moncarrera extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'moncarrera';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Carrera',
            'nombre' => 'Carrera',
        ];
    }
}
