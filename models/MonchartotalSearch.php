<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monchartotal;

/**
 * MonchartotalSearch represents the model behind the search form about `app\models\Monchartotal`.
 */
class MonchartotalSearch extends Monchartotal
{
    public function rules()
    {
        return [
            [['idpregunta'
                , 'pregunta'
                , 'cantidad'
                , 'respuesta'
                , 'idrespuesta'
                , 'idmateria'
                , 'materia'
                ,'idcarrera'
                , 'idencuesta'
                ,'encuesta'
                , 'fecha'
                ,'carrera', 'fecha'], 'safe'],            
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Monchartotal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }        
        

        return $dataProvider;
    }
}
