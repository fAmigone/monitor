<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monopcion".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $idpregunta
 *
 * @property Monpregunta $idpregunta0
 */
class Monopcion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monopcion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'idpregunta'], 'required'],
            [['idpregunta'], 'integer'],
            [['idrespuesta'], 'integer'],
            [['nombre'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Respuesta Libre',
            'idpregunta' => 'Pregunta',
            'idrespuesta' => 'Opción Contestada',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdpregunta0()
    {
        return $this->hasOne(Monpregunta::className(), ['id' => 'idpregunta']);
    }
    public function getIdrespuesta0()
    {
        return $this->hasOne(Monrespuesta::className(), ['id' => 'idrespuesta']);
    }
}
