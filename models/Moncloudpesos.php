<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "moncloudpesos".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $peso
 */
class Moncloudpesos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'moncloudpesos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'peso', 'idpregunta'], 'required'],
            [['peso'], 'integer'],            
            [['peso', 'idpregunta'], 'integer'],
            [['nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'nombre' => 'Nombre',
            'peso' => 'Peso',
            'idpregunta' => 'Idpregunta',
        ];
    }
    
    public function getPesos($idpregunta)
    {
    //<div id="wordcloud" style="border:1px solid #f00;height:150px;width:150px;">
    //    <span data-weight="14">word</span>
    //    <span data-weight="5">another</span>
    //    <span data-weight="7">things</span>
    //    <span data-weight="23">super</span>
    //    <span data-weight="10">cloud</span>
    //</div>
        
        
    }
}
