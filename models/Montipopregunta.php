<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "montipopregunta".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Monpregunta[] $monpreguntas
 */
class Montipopregunta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'montipopregunta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Tipo de Respuesta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonpreguntas()
    {
        return $this->hasMany(Monpregunta::className(), ['idtipo' => 'id']);
    }
}
