<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monmateria;

/**
 * MonmateriaSearch represents the model behind the search form about `app\models\Monmateria`.
 */
class MonmateriaSearch extends Monmateria
{
    public function rules()
    {
        return [
            [['id', 'idcarrera', 'idanio'], 'integer'],
            [['nombre'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Monmateria::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idcarrera' => $this->idcarrera,
            'idanio' => $this->idanio,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
