<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monpregunta".
 *
 * @property integer $id
 * @property integer $idencuesta
 * @property string $nombre
 * @property integer $idtipo
 *
 * @property Monopcion[] $monopcions
 * @property Montipopregunta $idtipo0
 * @property Monencuesta $idencuesta0
 */
class Monpregunta extends \yii\db\ActiveRecord
{
 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monpregunta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idencuesta', 'nombre', 'idtipo'], 'required'],
            [['idencuesta', 'idtipo'], 'integer'],
            [['nombre'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idencuesta' => 'Encuesta',
            'nombre' => 'Pregunta',
            'idtipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonopcions()
    {
        return $this->hasMany(Monopcion::className(), ['idpregunta' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdtipo0()
    {
        return $this->hasOne(Montipopregunta::className(), ['id' => 'idtipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdencuesta0()
    {
        return $this->hasOne(Monencuesta::className(), ['id' => 'idencuesta']);
    }
}
