<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monpregunta;

/**
 * MonpreguntaSearch represents the model behind the search form about `app\models\Monpregunta`.
 */
class MonpreguntaSearch extends Monpregunta
{
    public function rules()
    {
        return [
            [['id', 'idencuesta', 'idtipo'], 'integer'],
            [['nombre'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Monpregunta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idencuesta' => $this->idencuesta,
            'idtipo' => $this->idtipo,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
