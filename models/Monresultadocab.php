<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monresultadocab".
 *
 * @property integer $id
 * @property integer $idencuesta
 *
 * @property Monencuesta $idencuesta0
 */
class Monresultadocab extends \yii\db\ActiveRecord
{
    public $orden=0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monresultadocab';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idencuesta'], 'required'],
            [['idencuesta'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idencuesta' => 'Nombre de la Encuesta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdencuesta0()
    {
        return $this->hasOne(Monencuesta::className(), ['id' => 'idencuesta']);
    }
}
