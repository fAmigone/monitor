<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monrespuesta".
 *
 * @property integer $id
 * @property integer $idopcion
 * @property string $nombre
 */
class Monrespuesta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return 'monrespuesta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idopcion', 'nombre'], 'required'],
            [['idopcion'], 'integer'],
            [['nombre'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idopcion' => 'Opcion',
            'nombre' => 'Opción',
        ];
    }
}
