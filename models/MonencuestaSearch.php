<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monencuesta;

/**
 * MonencuestaSearch represents the model behind the search form about `app\models\Monencuesta`.
 */
class MonencuestaSearch extends Monencuesta
{
    public function rules()
    {
        return [
            [['id', 'idmateria', 'abierta'], 'integer'],
            [['nombre', 'fecha'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Monencuesta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idmateria' => $this->idmateria,
            'fecha' => $this->fecha,
            'abierta' => $this->abierta,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
