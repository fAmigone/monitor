<?php

namespace app\models;
use yii\helpers\Json;

use Yii;

/**
 * This is the model class for table "monencuesta".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $idmateria
 * @property string $fecha
 * @property integer $abierta
 *
 * @property Monmateria $idmateria0
 * @property Monpregunta[] $monpreguntas
 */
class Monencuesta extends \yii\db\ActiveRecord
{
    public $orden=0;
    public $idcarrera;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monencuesta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'idmateria', 'fecha', 'abierta'], 'required'],
            [['idmateria', 'abierta'], 'integer'],
            [['idcarrera'],'safe'],
            [['fecha'], 'safe'],
            [['nombre'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Encuesta',
            'idmateria' => 'Materia',
            'fecha' => 'Fecha',
            'abierta' => 'Habilitada',
            'idcarrera'=> 'Carrera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdmateria0()
    {
        return $this->hasOne(Monmateria::className(), ['id' => 'idmateria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonpreguntas()
    {
        return $this->hasMany(Monpregunta::className(), ['idencuesta' => 'id']);
    }
     public function getMonchartotal()
    {
        return $this->hasMany(Monchartotal::className(), ['idencuesta' => 'id']);
    }
    public function getData($idpregunta)
    {
        $chars = Monchartotal::find()
                ->where(['idencuesta' => $this->id])
                ->where(['idpregunta' => $idpregunta])
                ->all();
        

        $cad= [];

        foreach ($chars as $char)
        {
            //[["Muy bueno",6],["Normal",4],["Deficiente",7]]
  
            
            
                $cad[]= "[\"".$char->respuesta."\",".$char->cantidad."]";
         
        }
        
       // print_r('['.implode(",",$cad).']');exit;
        
        $cad='['.implode(",",$cad).']';
        //return $chars;
        //$code = \yii\helpers\Json::encode($chars);
        return  $cad;
    }
}
