<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monmateria".
 *
 * @property integer $id
 * @property integer $idcarrera
 * @property integer $idanio
 * @property string $nombre
 *
 * @property Monencuesta[] $monencuestas
 */
class Monmateria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monmateria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcarrera', 'idanio', 'nombre'], 'required'],
            [['idcarrera', 'idanio'], 'integer'],
            [['nombre'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Materia',
            'idcarrera' => 'Carrera',
            'idanio' => 'Año',
            'nombre' => 'Materia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonencuestas()
    {
        return $this->hasMany(Monencuesta::className(), ['idmateria' => 'id']);
    }
}
