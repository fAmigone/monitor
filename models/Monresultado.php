<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monresultado".
 *
 * @property integer $id
 * @property integer $idmonresultadocab
 * @property integer $idpregunta
 * @property integer $idrespuesta
 * @property string $libre
 *
 * @property Monpregunta $idpregunta0
 * @property Monresultadocab $idmonresultadocab0
 * @property Monrespuesta $idrespuesta0
 */
class Monresultado extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monresultado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idmonresultadocab'], 'required'],
            [['idmonresultadocab', 'idpregunta', 'idrespuesta'], 'integer'],
            [['libre'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idmonresultadocab' => 'Idmonresultadocab',
            'idpregunta' => 'Idpregunta',
            'idrespuesta' => 'Respuesta',
            'libre' => 'Respuesta Libre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdpregunta0()
    {
        return $this->hasOne(Monpregunta::className(), ['id' => 'idpregunta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdmonresultadocab0()
    {
        return $this->hasOne(Monresultadocab::className(), ['id' => 'idmonresultadocab']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdrespuesta0()
    {
        return $this->hasOne(Monrespuesta::className(), ['id' => 'idrespuesta']);
    }
    public function afterSave($insert) {
        parent::afterSave($insert);
        //si es texto libre, entonces peso
        if ($this->idpregunta0->idtipo = 2) {
            //guardamos el contenido a pesar en un string
            $lista= $this->libre;
            //reemplazamos todos los {; , . /} por espacio en blanco 
            $lista= str_replace(',', ' ', $lista);
            $lista= str_replace(';', ' ', $lista);
            $lista= str_replace(':', ' ', $lista);
            $lista= str_replace('.', ' ', $lista);
            $lista= str_replace('/', ' ', $lista);
            $lista= str_replace('?', ' ', $lista);
            $lista= str_replace('¿', ' ', $lista);
            $lista= str_replace('¡', ' ', $lista);
            $lista= str_replace('!', ' ', $lista);
            $lista= str_replace('"', ' ', $lista);
            //minuscula
            $lista = strtolower($lista);
            //ponemos cada palabra en un arreglo de strings            
            $palabras = explode(" ", $lista);                       
            //recorremos el arreglo pesando cada palabra
            foreach ($palabras as $palabra)
            {
                $unaPalabra= new Moncloudpesos();
                $unaPalabra->nombre= $palabra;
                //debo ver que no pertenezca a la lista de stopwords
                $existe= Monstopwords::findOne($unaPalabra->nombre);                                            
                if ($existe==NULL)
                {   
                    $cloudword= Moncloudpesos::findOne(['nombre'=>$unaPalabra->nombre, 'idpregunta'=>$this->idpregunta]); 
                    if ($cloudword!=NULL){
                        $cloudword->peso=$cloudword->peso+1;                        
                    }
                    else {
                        $cloudword = new Moncloudpesos;
                        $cloudword->nombre = $unaPalabra->nombre;
                        $cloudword->peso = 1;
                        $cloudword->idpregunta = $this->idpregunta;                                            
                    }
                    $cloudword->save();
                }                
            }
        }
    }
}
