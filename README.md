# ***Sistema de Monitoreo de Calidad Percibida.*** 

## **Tablero gráfico comparativo de los resultados de la Encuesta de Calidad realizada a todos los alumnos de todas las materias de todos los años. De manera que será posible comparar los resultados entre años, materias, departamentos  y carreras de la facultad.
Estratégicamente, hoy no sabemos que opinan los alumnos de la Facultad. Con ésta herramienta, aunque simple, el Sec. Académico de la facultad tendrá una base empírica para la toma de decisiones referidas a la evaluación de satisfacción por parte del alumnado.
** 

### Resuelto ###

- Creación de la Encuesta
- Selección de la encuesta a responder


### Roadmap ###
- Contestación de la encuesta
- Renderización de resultados
- Opciones de Comparativas