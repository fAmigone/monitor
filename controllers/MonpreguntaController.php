<?php

namespace app\controllers;

use Yii;
use app\models\Monpregunta;
use app\models\MonpreguntaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MonpreguntaController implements the CRUD actions for Monpregunta model.
 */
class MonpreguntaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monpregunta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonpreguntaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Monpregunta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new \app\models\MonrespuestaSearch;
        $dataProvider = $searchModel->search(['MonrespuestaSearch'=>['idopcion'=>$id]]);
        $modelRespuesta = $this->crearRespuesta($id);
        
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelRespuesta' => $modelRespuesta,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
   protected function crearRespuesta($id) {
        $model = new \app\models\Monrespuesta;
        $model->idopcion = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //flash
        }
        return $model;
    }
    /**
     * Creates a new Monpregunta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Monpregunta;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Monpregunta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Monpregunta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monpregunta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Monpregunta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monpregunta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
