<?php

namespace app\controllers;

use Yii;
use app\models\Monresultadocab;
use app\controllers\MonresultadocabSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MonresultadocabController implements the CRUD actions for Monresultadocab model.
 */
class MonresultadocabController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monresultadocab models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonresultadocabSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Monresultadocab model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = new Monresultadocab;
        //$model->idencuesta=$id;
        //$model->save();

        //$modelEncuesta = new \app\models\Monencuesta ;        
        //$modelEncuesta = \app\models\Monencuesta::findOne($id);
        $modelResultado = $this->crearResultado($model->id);
        $modelResultado->idpregunta = 1;//$modelEncuesta->monpreguntas[$modelEncuesta->orden]->id;
        return $this->render('view', [
            //'model' => $model,
           // 'modelEncuesta' => $modelEncuesta,
            'model' => $this->findModel($id),
            'modelResultado' =>$modelResultado, 
            
        ]);
    }
   protected function crearResultado($id) {
        $model = new \app\models\Monresultado;
        $model->idmonresultadocab = $id;
         if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //flash
        }
        return $model;
    }
    /**
     * Creates a new Monresultadocab model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Monresultadocab;
        $model->idencuesta=$id;
        //$model->save();
        
        

        if ( $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelEncuesta'=> $modelEncuesta,
            ]);
        }
    }

    /**
     * Updates an existing Monresultadocab model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Monresultadocab model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monresultadocab model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Monresultadocab the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monresultadocab::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
