<?php

namespace app\controllers;

use Yii;
use app\models\Monresultadocab;
use app\models\MonresultadocabSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Session;

/**
 * MonresultadocabController implements the CRUD actions for Monresultadocab model.
 */
class MonresultadocabController extends Controller
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monresultadocab models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonresultadocabSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Monresultadocab model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //modelo
        $model = $this->findModel($id);
        //busco la encuesta para obtener las preguntas
        $modelEncuesta = new \app\models\Monencuesta ;        
        $modelEncuesta = \app\models\Monencuesta::findOne($model->idencuesta);        
        //abro la sesion para recuperar el id de pregunta
        $session = new Session;
        $session->open();   
        $totalPreguntas = count($modelEncuesta->monpreguntas);
        if ($session['orden'] <= $totalPreguntas)
        {           
            //seteo el resultado o contestacion
            $idpregunta = $modelEncuesta->monpreguntas[$session['orden']-1]->id;
            $modelResultado = $this->crearResultado($id, $idpregunta);
            if (($session['orden']-1)<$totalPreguntas){
                $modelResultado->idpregunta = $modelEncuesta->monpreguntas[$session['orden']-1]->id;
            }
            else 
            {
                return $this->render('viewok', [
                'model' => $model,                
            ]);
            }
            return $this->render('view', [
                'model' => $model,
                'modelResultado'=>$modelResultado,
                'orden'=>$model->orden,
                'total'=>$totalPreguntas,
            ]);
        }
        else {
            return $this->render('viewok', [
                'model' => $model,                
            ]);
        }
    }
   protected function crearResultado($id, $idpregunta) {
 
        $modelResul = new \app\models\Monresultado;
        $modelResul->idmonresultadocab = $id;                
        $modelResul->idpregunta = $idpregunta;
        
         if ($modelResul->load(Yii::$app->request->post()) && $modelResul->save()) {
            $session = new Session;
            $session->open();        
            $session['orden'] = $session['orden']+1;  
            
            
        }
        return $modelResul;
    }
    /**
     * Creates a new Monresultadocab model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate($id)
    {

        $model = new Monresultadocab;
        
        //this->modelEncuesta = \app\models\Monencuesta::findOne($id);
        //this->modelEncuesta->orden = 0;
        $model->idencuesta=$id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                
            ]);
        }
    }

    /**
     * Updates an existing Monresultadocab model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Monresultadocab model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monresultadocab model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Monresultadocab the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monresultadocab::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}