<?php

namespace app\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monresultadocab;

/**
 * MonresultadocabSearch represents the model behind the search form about `app\models\Monresultadocab`.
 */
class MonresultadocabSearch extends Monresultadocab
{
    public function rules()
    {
        return [
            [['id', 'idencuesta'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Monresultadocab::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idencuesta' => $this->idencuesta,
        ]);

        return $dataProvider;
    }
}
