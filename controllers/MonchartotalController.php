<?php

namespace app\controllers;

use Yii;
use app\models\Monchartotal;
use app\models\MonchartotalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MonchartotalController implements the CRUD actions for Monchartotal model.
 */
class MonchartotalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monchartotal models.
     * @return mixed
     */
    public function actionIndex($id)
    {   //$id=3;
        $laEncuesta = \app\models\Monencuesta::findOne($id);
        $laspreguntas = \app\models\Monpregunta::find()->where('idencuesta=:idencuesta', [':idencuesta'=>$id])->andwhere('idtipo=:idtipo', [':idtipo'=>1])->distinct()->all();
        $laspreguntasLibres = \app\models\Monpregunta::find()->where('idencuesta=:idencuesta', [':idencuesta'=>$id])->andwhere('idtipo=:idtipo', [':idtipo'=>2])->distinct()->all();
       

        return $this->render('index', [
         //   'dataProvider' => $dataProvider,
           // 'searchModel' => $searchModel,
            'encuesta' => $laEncuesta,
            'preguntas' => $laspreguntas,
            'preguntasLibres' => $laspreguntasLibres,
        ]);
    }
 public function actionIndexmonitor()
    {   $id=3;
        $laEncuesta = \app\models\Monencuesta::findOne($id);
        $laspreguntas = \app\models\Monpregunta::find()->where('idencuesta=:idencuesta', [':idencuesta'=>$id])->distinct()->all();
       

        return $this->render('indexmonitor', [
         //   'dataProvider' => $dataProvider,
           // 'searchModel' => $searchModel,
            'encuesta' => $laEncuesta,
            'preguntas' => $laspreguntas,
        ]);
    }
    /**
     * Displays a single Monchartotal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Monchartotal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Monchartotal;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Monchartotal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Monchartotal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monchartotal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Monchartotal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monchartotal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
  
}
