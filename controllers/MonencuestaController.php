<?php

namespace app\controllers;

use Yii;
use app\models\Monencuesta;
use app\models\MonencuestaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Session;
/**
 * MonencuestaController implements the CRUD actions for Monencuesta model.
 */
class MonencuestaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monencuesta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonencuestaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $session = new Session;
        $session->open();
        $session['orden'] = 1;
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    public function actionIndexadmin()
    {
        $searchModel = new MonencuestaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('indexadmin', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    /**
     * Displays a single Monencuesta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        $searchModelPregunta = new \app\models\MonpreguntaSearch;        
        $dataProviderPregunta = $searchModelPregunta->search(['MonpreguntaSearch'=>['idencuesta'=>$id]]);
        $modelPregunta = $this->crearPregunta($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelPregunta' => $modelPregunta,
            'dataProviderPregunta' => $dataProviderPregunta,
            'searchModelPregunta' => $searchModelPregunta,
        ]);
    }
   protected function crearPregunta($id) {
        $model = new \app\models\Monpregunta;
        $model->idencuesta = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //flash
        }
        return $model;
    }
    /**
     * Creates a new Monencuesta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Monencuesta;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,                
            ]);
        }
    }

    /**
     * Updates an existing Monencuesta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Monencuesta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monencuesta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Monencuesta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monencuesta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
