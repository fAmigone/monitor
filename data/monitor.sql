-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-08-2014 a las 16:10:14
-- Versión del servidor: 5.5.35
-- Versión de PHP: 5.4.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `yii2basic`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monanio`
--

CREATE TABLE IF NOT EXISTS `monanio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `monanio`
--

INSERT INTO `monanio` (`id`, `nombre`) VALUES
(1, 'Primero'),
(2, 'Segundo'),
(3, 'Tercero'),
(4, 'Cuarto'),
(5, 'Quinto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moncarrera`
--

CREATE TABLE IF NOT EXISTS `moncarrera` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `moncarrera`
--

INSERT INTO `moncarrera` (`id`, `nombre`) VALUES
(4, 'Licenciatura en Turismo'),
(5, 'Guia de Turismo');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `monchartotal`
--
CREATE TABLE IF NOT EXISTS `monchartotal` (
`idpregunta` int(11)
,`pregunta` varchar(500)
,`idrespuesta` int(11)
,`respuesta` varchar(2000)
,`cantidad` bigint(21)
,`carrera` varchar(300)
,`materia` varchar(300)
,`idencuesta` int(11)
,`encuesta` varchar(200)
,`fecha` varchar(15)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `monchartotalsub`
--
CREATE TABLE IF NOT EXISTS `monchartotalsub` (
`idpregunta` int(11)
,`idrespuesta` int(11)
,`cantidad` bigint(21)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monencuesta`
--

CREATE TABLE IF NOT EXISTS `monencuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `idmateria` int(11) NOT NULL,
  `fecha` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `abierta` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idmateria` (`idmateria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `monencuesta`
--

INSERT INTO `monencuesta` (`id`, `nombre`, `idmateria`, `fecha`, `abierta`) VALUES
(1, 'Encuesta de Calidad Educativa', 16, '12/24/1900', 1),
(2, 'Una encuesta', 16, '0000-00-00', 1),
(3, 'Otraaaaa', 16, '07/31/2014', 1),
(4, 'Encuesta de Satisfacción Taller Libertya', 16, '08/26/2014', 1),
(5, 'Encuesta Final', 17, '08/20/2014', 1),
(6, 'Encuesta De Maestría', 16, '08/14/2014', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monmateria`
--

CREATE TABLE IF NOT EXISTS `monmateria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcarrera` int(11) NOT NULL,
  `idanio` int(11) NOT NULL,
  `nombre` varchar(300) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idcarrera` (`idcarrera`,`idanio`),
  KEY `idcarrera_2` (`idcarrera`),
  KEY `idanio` (`idanio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `monmateria`
--

INSERT INTO `monmateria` (`id`, `idcarrera`, `idanio`, `nombre`) VALUES
(16, 4, 1, 'Licenciatura Introductoria'),
(17, 5, 1, 'Guia Introductoria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monpregunta`
--

CREATE TABLE IF NOT EXISTS `monpregunta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idencuesta` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `idtipo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idencuesta` (`idencuesta`,`idtipo`),
  KEY `idtipo` (`idtipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `monpregunta`
--

INSERT INTO `monpregunta` (`id`, `idencuesta`, `nombre`, `idtipo`) VALUES
(1, 1, 'Cual es el color del caballo blanco de San Martín?', 1),
(2, 1, 'Cual es el color mas lindo?', 1),
(3, 1, 'Cual es el color mas lindo?', 1),
(4, 1, 'una de texto libre', 2),
(5, 1, 'Esta es otra pregunta', 1),
(6, 1, 'sdafasdf', 1),
(7, 1, 'nvnvvn', 1),
(8, 5, 'Como calificaría la aplicabilidad de los contenidos?', 1),
(9, 5, 'Por favor, asignarle una evaluación al desempeño del docente.', 1),
(10, 6, 'En que año gano el Sabalero la Libertadores?', 1),
(11, 6, 'En que año gano la Sudamericana Colon de Santa Fe??', 1),
(12, 6, 'En que año gano el torneo local Colon de Santa Fe??', 1),
(13, 3, 'pregunta uno', 1),
(14, 3, 'pregunta dos', 1),
(15, 3, 'pregunta tres', 1),
(16, 3, 'pregunta cuatro', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monrespuesta`
--

CREATE TABLE IF NOT EXISTS `monrespuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idopcion` int(11) NOT NULL,
  `nombre` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idopcion` (`idopcion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `monrespuesta`
--

INSERT INTO `monrespuesta` (`id`, `idopcion`, `nombre`) VALUES
(1, 1, 'Verdeagua'),
(2, 1, 'Azul'),
(3, 8, 'Poco Aplicables'),
(4, 8, 'Aplicables'),
(5, 8, 'Muy Aplicables'),
(6, 9, 'Malo'),
(7, 9, 'Bueno'),
(8, 9, 'Regular'),
(9, 10, '1960'),
(10, 10, '1980'),
(11, 10, 'Nunca'),
(12, 11, '1970'),
(13, 11, '2000'),
(14, 11, 'Nunca'),
(15, 12, '1988'),
(16, 12, '2005'),
(17, 12, 'Nunca'),
(18, 13, 'uno uno'),
(19, 13, 'uno dos'),
(20, 13, 'uno tres'),
(21, 14, 'dos uno'),
(22, 14, 'dos dos'),
(23, 14, 'dos tres'),
(24, 15, 'tres uno'),
(25, 15, 'tres dos'),
(26, 15, 'tres tres'),
(27, 16, 'cuatro uno'),
(28, 16, 'cuatro dos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monresultado`
--

CREATE TABLE IF NOT EXISTS `monresultado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idmonresultadocab` int(11) NOT NULL,
  `idpregunta` int(11) DEFAULT NULL,
  `idrespuesta` int(11) DEFAULT NULL,
  `libre` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idmonresultadocab` (`idmonresultadocab`,`idpregunta`,`idrespuesta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=138 ;

--
-- Volcado de datos para la tabla `monresultado`
--

INSERT INTO `monresultado` (`id`, `idmonresultadocab`, `idpregunta`, `idrespuesta`, `libre`) VALUES
(126, 245, 13, 18, NULL),
(127, 245, 14, 21, NULL),
(128, 245, 15, 24, NULL),
(129, 245, 16, 27, NULL),
(130, 246, 13, 18, NULL),
(131, 246, 14, 21, NULL),
(132, 246, 15, 24, NULL),
(133, 246, 16, 27, NULL),
(134, 247, 13, 19, NULL),
(135, 247, 14, 22, NULL),
(136, 247, 15, 25, NULL),
(137, 247, 16, 28, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monresultadocab`
--

CREATE TABLE IF NOT EXISTS `monresultadocab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idencuesta` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idencuesta` (`idencuesta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=248 ;

--
-- Volcado de datos para la tabla `monresultadocab`
--

INSERT INTO `monresultadocab` (`id`, `idencuesta`) VALUES
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(183, 1),
(186, 1),
(187, 1),
(190, 1),
(191, 1),
(192, 1),
(194, 1),
(195, 1),
(204, 1),
(226, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(12, 2),
(13, 2),
(14, 2),
(113, 2),
(227, 2),
(228, 3),
(229, 3),
(230, 3),
(231, 3),
(232, 3),
(233, 3),
(234, 3),
(235, 3),
(236, 3),
(237, 3),
(238, 3),
(239, 3),
(240, 3),
(241, 3),
(242, 3),
(243, 3),
(244, 3),
(245, 3),
(246, 3),
(247, 3),
(182, 5),
(184, 5),
(185, 5),
(188, 5),
(189, 5),
(193, 5),
(196, 5),
(197, 5),
(198, 5),
(199, 5),
(200, 5),
(201, 5),
(202, 5),
(203, 5),
(206, 5),
(207, 5),
(208, 5),
(209, 5),
(210, 5),
(211, 5),
(212, 5),
(213, 5),
(214, 5),
(215, 5),
(216, 5),
(218, 5),
(219, 5),
(220, 5),
(221, 5),
(222, 5),
(223, 5),
(224, 5),
(225, 5),
(205, 6),
(217, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `montipopregunta`
--

CREATE TABLE IF NOT EXISTS `montipopregunta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `montipopregunta`
--

INSERT INTO `montipopregunta` (`id`, `nombre`) VALUES
(1, 'Opciones Múltiples'),
(2, 'Texto Libre');

-- --------------------------------------------------------

--
-- Estructura para la vista `monchartotal`
--
DROP TABLE IF EXISTS `monchartotal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `monchartotal` AS select `base`.`idpregunta` AS `idpregunta`,`monpregunta`.`nombre` AS `pregunta`,`base`.`idrespuesta` AS `idrespuesta`,`monrespuesta`.`nombre` AS `respuesta`,`base`.`cantidad` AS `cantidad`,`moncarrera`.`nombre` AS `carrera`,`monmateria`.`nombre` AS `materia`,`monencuesta`.`id` AS `idencuesta`,`monencuesta`.`nombre` AS `encuesta`,`monencuesta`.`fecha` AS `fecha` from (((((`monchartotalsub` `base` join `monpregunta` on((`monpregunta`.`id` = `base`.`idpregunta`))) join `monrespuesta` on((`monrespuesta`.`id` = `base`.`idrespuesta`))) join `monencuesta` on((`monencuesta`.`id` = `monpregunta`.`idencuesta`))) join `monmateria` on((`monmateria`.`id` = `monencuesta`.`idmateria`))) join `moncarrera` on((`moncarrera`.`id` = `monmateria`.`idcarrera`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `monchartotalsub`
--
DROP TABLE IF EXISTS `monchartotalsub`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `monchartotalsub` AS select `monresultado`.`idpregunta` AS `idpregunta`,`monresultado`.`idrespuesta` AS `idrespuesta`,count(0) AS `cantidad` from `monresultado` group by `monresultado`.`idpregunta`,`monresultado`.`idrespuesta`;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `monencuesta`
--
ALTER TABLE `monencuesta`
  ADD CONSTRAINT `monencuesta_ibfk_1` FOREIGN KEY (`idmateria`) REFERENCES `monmateria` (`id`);

--
-- Filtros para la tabla `monpregunta`
--
ALTER TABLE `monpregunta`
  ADD CONSTRAINT `monpregunta_ibfk_1` FOREIGN KEY (`idencuesta`) REFERENCES `monencuesta` (`id`),
  ADD CONSTRAINT `monpregunta_ibfk_2` FOREIGN KEY (`idtipo`) REFERENCES `montipopregunta` (`id`);

--
-- Filtros para la tabla `monresultadocab`
--
ALTER TABLE `monresultadocab`
  ADD CONSTRAINT `monresultadocab_ibfk_1` FOREIGN KEY (`idencuesta`) REFERENCES `monencuesta` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;