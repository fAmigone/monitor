<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var app\models\Monencuesta $model
 */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Encuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monencuesta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'idmateria0.nombre',
            'fecha',
            'abierta',            
        ],
    ]) ?>

        <?php 
        
        echo '<h2>Agregar Pregunta </h2>';  
        $modelPregunta->nombre = NULL;
        $modelPregunta->idtipo = NULL;        
        
        echo $this->render('_formPregunta', [
        'model' => $modelPregunta
      ]);
        echo '<hr>'; ?>
      <?php echo '<h2>Preguntas de la Encuesta</h2>'; ?> 
      <?= GridView::widget([
        'dataProvider' => $dataProviderPregunta,
        'filterModel' => $searchModelPregunta,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'idtipo',
            ['class' => 'yii\grid\ActionColumn', 
                  'controller'=>'monpregunta',],
        ],
    ]); ?>
    
</div>
