<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Monpregunta $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="monpregunta-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 500]) ?>

    <?= $form->field($model, 'idtipo')->dropDownList(ArrayHelper::map(\app\models\Montipopregunta::find()->all(),'id','nombre'))?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
