<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monencuesta $model
 */

$this->title = 'Modificar Encuesta: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Encuesta', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monencuesta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
