<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\widgets\SwitchInput;

/**
 * @var yii\web\View $this
 * @var app\models\Monencuesta $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="monencuesta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 200]) ?>

    
    <?php
    
    $dataCategory=ArrayHelper::map(\app\models\Moncarrera::find()->asArray()->all(), 'id', 'nombre');
    
    echo $form->field($model, 'idcarrera')->dropDownList($dataCategory, 
             ['prompt'=>'-Seleccione la Carrera-',
              'onchange'=>'
                $.post( "'.Yii::$app->urlManager->createUrl('monmateria/lists?id=').'"+$(this).val(), function( data ) {
                  $( "select#id" ).html(data);
                });
            ']); 
 
        $dataPost=ArrayHelper::map(\app\models\Monmateria::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'idmateria')
        ->dropDownList(            
            $dataPost,           
            ['id'=>'id',
             'prompt'=>'-Seleccione la Materia-',   ]
        ); 
    ?>
    
    
    
    <?= $form->field($model, 'fecha')->widget(DatePicker::classname(), [
	'options' => ['placeholder' => 'Ingrese la fecha'],
	'pluginOptions' => [
		'autoclose' => true
	]
]);
?>
    <?= $form->field($model, 'abierta')->widget(SwitchInput::classname(), []); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
