<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\MonencuestaSearch $searchModel
 */

$this->title = 'Encuestas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monencuesta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Encuesta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'idmateria0.nombre',
            'fecha',
            'abierta',
            
             ['class' => 'yii\grid\ActionColumn',
                           'template' => '{view}{update}{delete}{Monitorear}',
                'buttons' => [
                    'Monitorear' => function ($url, $model) {
                return Html::a('Monitorear', ['/monchartotal/index', 'id' => $model->id]);
            },
                ],
        ],
        ],
    ]); ?>

</div>
