<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\MonresultadoSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="monresultado-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'idmonresultadocab') ?>

    <?= $form->field($model, 'idpregunta') ?>

    <?= $form->field($model, 'idrespuesta') ?>

    <?= $form->field($model, 'libre') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
