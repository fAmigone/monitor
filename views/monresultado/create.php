<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monresultado $model
 */

$this->title = 'Create Monresultado';
$this->params['breadcrumbs'][] = ['label' => 'Monresultados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monresultado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
