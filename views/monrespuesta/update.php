<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monrespuesta $model
 */

$this->title = 'Modificar Opción: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Respuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monrespuesta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
