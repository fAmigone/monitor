<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monresultadocab $model
 */

$this->title = 'Por favor, seleccione la encuesta que desea contestar';

?>
<div class="monresultadocab-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
