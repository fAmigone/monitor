<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Monresultado $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="monresultado-form">

    <?php $form = ActiveForm::begin(); ?>
    <h2><?php
        $unaPregunta = \app\models\Monpregunta::findOne($model->idpregunta);
        echo $unaPregunta->nombre;
        echo  '</h2>';
   
     $libre = $unaPregunta->idtipo == 1;
   ?> 
    
    <?php     
        //echo "resul idrespuesta nombre ".$model->idrespuesta0->nombre;
        if ($libre) {        
        echo $form->field($model, 'idrespuesta')->dropDownList(ArrayHelper::map(\app\models\Monrespuesta::find()->where('idopcion=:idopcion', [':idopcion'=>$model->idpregunta])->all(),'id','nombre'));
                      } 
    ?>
    <?php 
        if (!$libre) {
        echo $form->field($model, 'libre')->textInput(['maxlength' => 2000]);
                      } 
    ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Siguiente' : 'Siguiente', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
