<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Monresultadocab $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="monresultadocab-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'idencuesta')->dropDownList(ArrayHelper::map(\app\models\Monencuesta::find()->where('id=:id', [':id'=>$model->idencuesta])->all(),'id','nombre'))?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Contestar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
