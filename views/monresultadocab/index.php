<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\MonresultadocabSearch $searchModel
 */

$this->title = 'Monresultadocabs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monresultadocab-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Monresultadocab', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'idencuesta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
