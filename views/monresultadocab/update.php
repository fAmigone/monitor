<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monresultadocab $model
 */

$this->title = 'Update Monresultadocab: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Monresultadocabs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monresultadocab-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
