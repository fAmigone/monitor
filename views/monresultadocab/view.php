<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\Session;
/**
 * @var yii\web\View $this
 * @var app\models\Monresultadocab $model
 */

$this->title = 'Monitor de Calidad Percibida';//$model->id;

?>
<div class="monresultadocab-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idencuesta0.nombre',            
        ],
    ]) ?>
    <?php 
        $session = new Session;
        $session->open();        
        $orden = $session['orden'];  
        $calc= $orden/$total*100;
        
    ?>
    <center> <h4><?= $orden ?> de <?= $total ?> Preguntas</h4></center>
        <div class="progress">
        <div class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $calc?>%;">
            <span class="sr-only"><?php echo $calc?> Completa</span>
        </div>
    </div>
    <?php
    //$session = new Session;
    //$session->open();        
    //echo 'El orden esta en '.$session['orden'];
    echo $this->render('_formResultado', [
        'model' => $modelResultado,
        
      ]); ?>
    
</div>