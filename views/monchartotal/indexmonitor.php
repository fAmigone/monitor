
<?php

use yii\helpers\Html;
use \yii\web\JqueryAsset;//use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;




/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var app\models\MonmateriaSearch $searchModel
*/

$this->title = 'Resultados por pregunta';
?>


<div class="monmateria-index">
    <h1><?= Html::encode($this->title) ?></h1>




<?php 
       // echo $encuesta->getData($encuesta->id);
                 
        //$laspreguntas = $preguntas->getMonpreguntas();
    //$preguntas = $dataProvider->getModels();
$cont=0;

   foreach ($preguntas as $unaPregunta) {
       $cont=$cont+1;
        //echo $unaPregunta->id;
        echo '<div class="col-sm-6 col-md-4">';
        echo Highcharts::widget([ 
           'options'=>'{
                "chart": {                
                        "type": "pie"
                    }, 
              "title": { "text":"'.$unaPregunta->nombre.'" },

              "yAxis": {
                 "title": { "text": "Fruit eaten" }
              },
               "plotOptions": {
                        "pie": {
                            "shadow": "false"
                        }
                    },
        "series": [{
                        "name": "Cantidad de Respuestas",                        
                        "data": '.$encuesta->getData($unaPregunta->id).',
                        "size": "30%",
                        "innerSize": "20%",
                        "showInLegend":true,
                        "dataLabels": {
                            "enabled": "false"
                        }
                    }]            
           }'
        ]);
        echo '</div>';
    }
    //"data": [["Muy bueno",6],["Normal",4],["Deficiente",7]],
    //"data": '.$encuesta->getData.',
     
    ?>
    
    
</div>



