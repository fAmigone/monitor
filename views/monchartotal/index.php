<?php

use yii\helpers\Html;
use \yii\web\JqueryAsset;//use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;




/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var app\models\MonmateriaSearch $searchModel
*/

$this->title = $encuesta->nombre;
?>


<div class="monmateria-index">
    <center><h1><?= Html::encode($this->title) ?></h1></center>




<?php 
    //retornamos los chars de cada pregunta que sea multipleChoice   
   foreach ($preguntas as $unaPregunta) {       
        //echo $unaPregunta->id;
        echo '<div class="col-sm-6 col-md-4">';
        echo Highcharts::widget([ 
           'options'=>'{
                "chart": {                
                        "type": "pie"
                    }, 
              "title": { "text":"'.$unaPregunta->nombre.'" },

              "yAxis": {
                 "title": { "text": "Fruit eaten" }
              },
               "plotOptions": {
                        "pie": {
                            "shadow": "false"
                        }
                    },
        "series": [{
                        "name": "Cantidad de Respuestas",                        
                        "data": '.$encuesta->getData($unaPregunta->id).',
                        "size": "30%",
                        "innerSize": "20%",
                        "showInLegend":true,
                        "dataLabels": {
                            "enabled": "false"
                        }
                    }]            
           }'
        ]);
        echo '</div>';        
    }
//necesito esta estructura
//        <div id="wordcloud" style="border:1px solid #f00;height:150px;width:150px;">
//            <span data-weight="14">word</span>
//            <span data-weight="5">another</span>
//            <span data-weight="7">things</span>
//            <span data-weight="23">super</span>
//            <span data-weight="10">cloud</span>
//        </div>
    //tengo un array de preguntas libres, las recorro
    //este id es necesario para generar distintos IDs 
    
    $cont=1;    
    foreach ($preguntasLibres as $unaPregunta) {
        echo '<div class="col-sm-6 col-md-4">';
        echo '<center><h3>' . $unaPregunta->nombre . '</h3></center>';
        echo '<div class="cloud" id="wordcloud' . $cont . '" style="border:0px solid #1C1C1C;height:300px;width:300px;">';
        $pesos = \app\models\Moncloudpesos::find()->where('idpregunta=:idpregunta', [':idpregunta' => $unaPregunta->id])->distinct()->all();
        //tengo un array de preguntas pesadas
        foreach ($pesos as $unPeso) {
            echo '<span data-weight="' . $unPeso->peso . '">' . $unPeso->nombre . '</span>';
        }
        echo '</div>';
        echo '</div>';
        $cont = $cont + 1;
}
$js= 'var settings = {"size" : {"grid" : 16},"options" : {"color" : "random-dark","printMultiplier" : 3},"font" : "Futura, Helvetica, sans-serif","shape" : "square"}; $( ".cloud" ).awesomeCloud( settings );';
    $this->registerJsFile( 'cloud.js', [JqueryAsset::className()]);        
    $this->registerJs($js);
    ?>

    
</div>



