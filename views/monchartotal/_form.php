<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Monmateria $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="monmateria-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idcarrera')->textInput() ?>

    <?= $form->field($model, 'idanio')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 300]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
