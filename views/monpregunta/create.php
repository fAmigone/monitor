<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monpregunta $model
 */

$this->title = 'Create Monpregunta';
$this->params['breadcrumbs'][] = ['label' => 'Monpreguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monpregunta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
