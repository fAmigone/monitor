<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monpregunta $model
 */

$this->title = 'Modificar Pregunta: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Preguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monpregunta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
