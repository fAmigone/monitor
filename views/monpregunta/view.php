<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var app\models\Monpregunta $model
 */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Preguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monpregunta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [            
            'idtipo0.nombre',
        ],
    ]) ?>
   <?php 
   
     if ($model->idtipo == 1) {
        
        echo '<h2>Agregar Opción </h2>';  
        $modelRespuesta->nombre = NULL;                
        
        echo $this->render('_formRta', [
        'model' => $modelRespuesta
        ]);
        
        echo '<hr>'; 
        
        echo '<h2>Opciones de la Pregunta</h2>'; 
    
        echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            ['class' => 'yii\grid\ActionColumn',
                         'controller'=>'monrespuesta'],
        ],
        ]);
     }
     
     echo Html::a('Volver a la Encuesta', ['monencuesta/view', 'id' => $model->idencuesta], ['class' => 'btn btn-primary']);
     ?>
</div>
