<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monstopwords $model
 */

$this->title = 'Nueva Palabra Vacía';
//$this->params['breadcrumbs'][] = ['label' => 'Monstopwords', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monstopwords-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
