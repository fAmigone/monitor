<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Monstopwords $model
 */

$this->title = 'Update Monstopwords: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Monstopwords', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->nombre]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monstopwords-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
